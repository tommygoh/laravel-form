<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use Redirect;

class VehicleController extends Controller
{

	var $client;

	function __construct(){
        // parent::__construct(); // needed when adding a constructor to a controller
        $this->client = new Client(['cookies' => true]);
    }

    public function getLta(Request $request) {
    	$jar = new CookieJar;
    	$client = new Client(['cookies' => $jar]);
        $res = $client->request('POST', 'https://onepay.onemotoring.com.sg/onepay/erp/getTermCondition.do', [
            'form_params' => [
                'numberType' => 'valVehicle',
                'txtVehicleNo' => $request->input('vehicle')
            ]
        ]);
        $contents = $res->getBody()->getContents();
		$token = substr($contents, 3070,32);
        $result = $client->request('POST', 'https://onepay.onemotoring.com.sg/onepay/erp/getRecordSummary.do', [
            'form_params' => [
                'org.apache.struts.taglib.html.TOKEN' => $token,
                'accept' => 'Accept'
            ]
        ]);
        $data = $result->getBody()->getContents();
        if (strpos($data, 'lblNoRecord') !== false) {
		    return redirect('result')->with('no', 'No LTA found');
		} else {
			$dom = new \DOMDocument();
			@$dom->loadHTML($data);
			$xpath = new \DOMXpath($dom);
			$result = $xpath->query('//font[@class="VehicleNumberTitle"]');
			$title = $xpath->query('//font[@class="VehiclDetailText"]');
			$key = "";
			$string = "";
			$titles = "";
			if ($result->length > 0) {
				for ($x = 0; $x <= $result->length - 1; $x++) {
				    if (preg_match('~[0-9]+~', $result->item($x)->nodeValue) and strpos($result->item($x)->nodeValue, '$') === false) {
				    	$key .= $result->item($x)->nodeValue;
				    	$key = str_replace(" ", "", $key);
				    	$key .= ",";
				    }
				}
				for ($x = 0; $x <= $result->length - 1; $x++) {
				    if (str_contains($result->item($x)->nodeValue, "$")) {
				    	$string .= $result->item($x)->nodeValue;
				    	$string .= ",";
				    }
				}
				$array = explode(",", $key);
				$values = explode(",", $string);
			}
			if ($title->length > 0) {
				for ($x = 0; $x <= $title->length - 1; $x++) {
					$titles .= $title->item($x)->nodeValue;
					$titles = str_replace(" ", "", $titles);
					$titles .= ",";
				}
				$notes = explode(",", $titles);
			}
			return redirect(route('result'))
			->with('array', $array)
			->with('values', $values)
			->with('notes', $notes);
		}
    }

    public function getRoadTax(Request $request) {
    	$client = new Client();
        $result = $client->request('POST', 'https://vrl.lta.gov.sg/lta/vrl/action/enquireRebateByPublicBeforeDeregInput?FUNCTION_ID=F0304009TT', [
        	'headers' => [
		        'User-Agent' => 'okhttp/3.8.1',
		        'Host' => 'vrl.lta.gov.sg',
		        'Origin' => 'https://vrl.lta.gov.sg',
		        'Referer' => 'https://vrl.lta.gov.sg/lta/vrl/action/enquireRebateBeforeDeRegByPublicMain?FUNCTION_ID=F0304009TT',
		        'Upgrade-Insecure-Requests' => '1',
		        'Cookie' => $request->input('cookie')

		    ]          
		    ,'form_params' => [
                'vehicleNo' => strtoupper($request->input('vehicle')),
                'intendedDeRegDate' => date("dmY"),
                'ownerId' => $request->input('ownerId'),
                'ownerIdType' => '7',
                'countryCd' => '',
                'exportStatus' => 'Yes',
                'agreeTc' => 'Y',
                'dispatch' => 'sendWithOutPaymentDetails',
                'captchaFrom' => 'F0304009TT',
                'captchaResponse' => $request->input('captcha')
            ]
        ]);
        $contents = $result->getBody()->getContents();
        $dom = new \DOMDocument();
		@$dom->loadHTML($contents);
		$xpath = new \DOMXpath($dom);
		if (strpos($contents, 'errorTxtRedBold12pt') !== false) {
			$error = $xpath->query('//td[@class="errorTxtRedBold12pt"]');
		    return redirect(route('result-vehicle-detail'))
			->with('no', $error->item(0)->nodeValue);
		}
		$title = $xpath->query('//label[@class="text-label"]');
		$result = $xpath->query('//p[@class="form-control-static"]');
		$titles = "";
		$results = "";
		if ($title->length > 0) {
			for ($x = 0; $x <= $title->length - 1; $x++) {
				$titles .= $title->item($x)->nodeValue;
		    	$titles .= ",";
			}
		}
		if ($result->length > 0) {
			for ($x = 0; $x <= $result->length - 1; $x++) {
				$results .= $result->item($x)->nodeValue;
		    	$results .= "+";
			}
		}
		$arrayTitle = explode(",", $titles);
		$arrayResult = explode("+", $results);
		return redirect(route('result-vehicle-detail'))
			->with('arrayTitle', $arrayTitle)
			->with('arrayResult', $arrayResult);
    }

    public function getForm() {
    	$client = new Client();

        $result1 = $client->request('GET', 'https://vrl.lta.gov.sg/lta/vrl/action/enquireRebateBeforeDeRegByPublicMain?FUNCTION_ID=F0304009TT', [
        	'headers' => [
		        'User-Agent' => 'okhttp/3.8.1'
		    ]
        ]);
        $contents1 = $result1->getBody()->getContents();
        $headers = $result1->getHeaders();
        preg_match('/<iframe.*src=\"(.*)\".*><\/iframe>/isU', $contents1, $matches);
        $url = "https://vrl.lta.gov.sg/lta/vrl/action/".$matches[1];
        $cookieStr = "";
        foreach ($result1->getHeader('Set-Cookie') as $cookie) {
		    $strings = explode(";",$cookie);
		    // print_r($strings);
		    $cookieStr .= $strings[0] ."; ";
		}

		$result2 = $client->request('GET', $url, [
        	'headers' => [
		        'User-Agent' => 'okhttp/3.8.1',
		        'Cookie' => $cookieStr
		    ]
        ]);

		$contents2 = $result2->getBody()->getContents();
		$ch2 = curl_init();

	    curl_setopt($ch2, CURLOPT_URL, $url);

	    curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch2,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
	    $img = curl_exec($ch2);
	    curl_close($ch2);

		return view('form', ['output' => $contents2, 'cookie' => $cookieStr]);
    }
}
