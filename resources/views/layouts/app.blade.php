<!DOCTYPE html>
<html>
<head>
	<title>Form</title>
	<link rel="stylesheet" href="/css/app.css">
	<script src="https://unpkg.com/@ungap/custom-elements-builtin"></script>
	<script type="module" src="https://unpkg.com/x-frame-bypass"></script>
</head>
<body>
	@include('layouts.navbar')

	<div class="container"> 
		<div class="row">
			<div class="col-md-8 col-lg-8">
				@yield('content')
			</div>
		</div>
	</div>
</body>
</html>