<nav class="site-header py-1">
  <div class="container d-flex flex-column flex-md-row justify-content-between">
    <a class="py-2" href="#">
      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="d-block mx-auto" role="img" viewBox="0 0 24 24" focusable="false"><title>Product</title><circle cx="12" cy="12" r="10"/><path d="M14.31 8l5.74 9.94M9.69 8h11.48M7.38 12l5.74-9.94M9.69 16L3.95 6.06M14.31 16H2.83m13.79-4l-5.74 9.94"/></svg>
    </a>
    <a class="py-2 d-none d-md-inline-block" href="/">Road Tax</a>
    <a class="py-2 d-none d-md-inline-block" href="/lta">LTA</a>
    <a class="py-2 d-none d-md-inline-block" href="#">Vehicle Detail</a>
    <a class="py-2 d-none d-md-inline-block" href="/acknowledge-receipt">AR</a>
    <a class="py-2 d-none d-md-inline-block" href="/sales-agreement">SA</a>
    <a class="py-2 d-none d-md-inline-block" href="/purchase-agreement">PA</a>
    <!-- <a class="py-2 d-none d-md-inline-block" href="/aml">AML</a> -->
    <!-- <a class="py-2 d-none d-md-inline-block" href="/company-authorization-letter">CAL</a> -->
    <!-- <a class="py-2 d-none d-md-inline-block" href="/company-transfer-authorization">CTA</a> -->
    <!-- <a class="py-2 d-none d-md-inline-block" href="/full-settlement-form">FTF</a> -->
    <a class="py-2 d-none d-md-inline-block" href="/invoice-full">IF</a>
    <a class="py-2 d-none d-md-inline-block" href="/commission-invoice">CI</a>
    <a class="py-2 d-none d-md-inline-block" href="/vehicle-collection-form">VCF</a>
    <a class="py-2 d-none d-md-inline-block" href="/vehicle-assessment-report">VAR</a>
  </div>
</nav>