@extends('layouts.app')

@section('content')
	@if(session('no'))
		<div class="alert alert-success">
			{{session('no')}}
		</div>
	@endif
	@if(session('array'))
		<table class="table">
			@php $count= 0; @endphp
		    @for ($x = 0; $x <= count(session('array')) - 1; $x++)
		        @if (strpos(session('array')[$x], '1.') !== false)
		        	<tr>
		        		<td> 
		        			{{session('notes')[$count]}}
		        			@php $count++; @endphp
		        		</td>
		        	</tr>
				    <tr>
				    	 <td>
					        Notice Number
					    </td>
					    <td>
					        Amount (SGD)
					    </td>
				    </tr>
				    <tr>
				    	 <td>
					        {{session('array')[$x]}}
					    </td>
					    <td>
					        {{session('values')[$x]}}
					    </td>
				    </tr>
				 @else
				 	<tr>
				    	 <td>
					        {{session('array')[$x]}}
					    </td>
					    <td>
					        {{session('values')[$x]}}
					    </td>
				    </tr>
				@endif
		    @endfor
		</table>
	@endif
@endsection