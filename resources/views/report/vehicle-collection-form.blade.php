<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="/css/app.css">
	<link rel="stylesheet" href="/css/report.css">
</head>
<body>
	<page>
		
		<header>
			<h1><b>ALL MOTORING PTE LTD</b></h1>
			<h3><b>48 TOH GUAN ROAD EAST #06-99 ENTERPRISE HUB S(608586)</b></h3>
			<h5>SPECIALISE IN LUXURY, SUPERCARDS EXPORT/TRADING & PARTS FOR PORSCHE, FERRARI, MASERATI</h5>
			<h6>HP: 8100 0999 TEL : 6464 6499 FAX: 64646477</h6>

		</header>
		<main>
			
			<h4 class="title-underline"><b>COLLECTION OF VEHICLE</b></h4>

			<p class="mt20"><b>Vehicle Particulars:</b></p>

			<p class="mt20"><b>Registration Number : XXXXXXXXXXXXXXXXXXXXXXXX Make/Model : XXXXXXXXXXXXXXXXXXXXXXXX Sales Contract No. : XXXXXXXXXXXXXXXXXXXXXXXX</b></p>

			<p class="mt20">Vehicle handed over to on XXXXXXXXXXXXXXXXXXXXXXXX (Date) @ XXXXXXXXXXXXXXXXX (Time) to XXXXXXXXXXXXXXXXXXXXXXXX (Name) XXXXXXXXXXXXXXXXXXXXXXXX (NRIC/Passport) XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX (Address)</p>

			<ul class="mt20">
			  <li>I shall be responsible and liable and shall fully indemnify M/S ALLMOTORING.SG for any damage, loss, fines, penalties, liability, costs and expenses incurred or suffered by them arising from, as a result of in connection with the vehicle after I have taken delivery (whether the Vehicle has already has been transferred to my name or otherwise).</li>
			  <li>I am fully aware that the Vehicle is not insured and shall take steps to insure the vehicle.</li>
			  <li>I hereby acknowledge and confirm that the Vehicle is sold to me on a "as is, where is" condition and that I have inspected and tested the Vehicle and am satisfied with and have accepted the actual state and condition thereof (including the odometer and all accessories thereto) and that I have relied wholly and solely on my judgement in purchasing the Vehicle and shall not raise or be entitled to raise any objection whatsoever hereafter.</li>
			  <li>I fully understand that no guarantee and/or warranty whatsoever is being given by M/S ALLMOTORING.SG whether express or implied, as to the state, quality, fitness of the Vehicle or any part thereof.</li>
			</ul>  

			<p class="mt20">Remarks : XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX</p>

			<table class="table table-borderless">
				<tr>
					<td class="w40"></td>
					<td class="w20"></td>
					<td class="w40">I hereby agree with the terms and conditions shown above</td>
				</tr>
				<tr>
					<td class="w40">
						<div class="pt80 text-underline">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</div>
					</td>
					<td class="w20"></td>
					<td class="w40">
						<div class="pt80 text-underline">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</div>
					</td>
				</tr>
				<tr>
					<td class="w40">Sales Consultant Signature</td>
					<td class="w20"></td>
					<td class="w40">Signature of Person Collecting</td>
				</tr>
				<tr>
					<td class="w40">Name : XXXXXXXXXXXXXXXXXXXXXXXX</td>
					<td class="w20"></td>
					<td class="w40"></td>
				</tr>

			</table>
		
		</main>	

	</page>
	
</body>

</html>