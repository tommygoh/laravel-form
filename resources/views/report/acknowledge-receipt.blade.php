<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="/css/app.css">
	<link rel="stylesheet" href="/css/report.css">
</head>
<body>
	<page>
		
		<header>
			<h1><b>ALL MOTORING PTE LTD</b></h1>
			<h3><b>48 TOH GUAN ROAD EAST #06-99 ENTERPRISE HUB S(608586)</b></h3>
			<h5>SPECIALISE IN LUXURY, SUPERCARDS EXPORT/TRADING & PARTS FOR PORSCHE, FERRARI, MASERATI</h5>
			<h6>HP: 8100 0999 TEL : 6464 6499 FAX: 64646477</h6>

		</header>
		<main>
			<div class="display-inline">
				<p>Date</p>
			</div>
			<div class="display-inline">
				<p>04-Jan-19</p>
			</div>
			<h5 class="mt10"><b>KENSO LEASING PTE LTD<b></h5>

			<p>NO 151 CHIN SWEE ROAD</p>
			<p>MANHATTAN HOUSE</p>
			<p>#13-07 SINGAPORE 169876</p>

			<h3 class="title-underline"><b>ACKNOWLEDGE RECEIPT</b></h3>

			<div class="mt20">
				<form class="form-inline">
					<div class="col-md-6">
						<div class="form-group">
							<label>Make & Model</label>

						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<p class="form-control-static">					
								HONDA CIVIC 1.8L 5AT
							</p>

						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label>Registration Number</label>

						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<p class="form-control-static">					
								SKH4141C
							</p>

						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label>Chassis Number</label>

						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<p class="form-control-static">					
								JHMFD16309S200825
							</p>

						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label>Engine Number</label>

						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<p class="form-control-static">					
								R18A14004918
							</p>

						</div>
					</div>

				</form>
			</div>

			<p class="mt40">This is confirm that we have sold and received full payment for the above mentioned vehicle.</p>

			<p class="title-overline">Authorized Signature</p>
			
		</main>	

	</page>
	
</body>

</html>