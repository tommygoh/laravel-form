<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="/css/app.css">
	<link rel="stylesheet" href="/css/report.css">
</head>
<body>
	<main>
		<table class="table table-bordered">
		<tr>
			<td colspan="2" class="center"><b>ANTI-MONEY LAUNDERING DECLARATION ("AML")</b></td>
		</tr>
		<tr>
			<td colspan="2">I declare and confirm that I am not a Politically-Exposed Person ("PEP") nor have I committed or been convicted or been investigated or am being investigated for any crimes or risks in relation to Money Laundering, tax evasion, and/or terrorism financing under Singapore Law or any other laws.<br /><br /><br /></td>
		</tr>

		<tr>
			<td class="center"><b>HIRER</b></td>
			<td class="center"><b>GUARANTOR</b></td>
		</tr>

		<tr>
			<td class="w50">Acknowledged by the above by:<br /><br />

			<table class="table table-borderless">
				<tr>
					<td>Signature :</td>
					<td>XXXXXXXXXXXX</td>
				</tr>
				<tr>
					<td>Name :</td>
					<td>XXXXXXXXXXXX</td>
				</tr>
				<tr>
					<td>NRIC / Passport No :</td>
					<td>XXXXXXXXXXXX</td>
				</tr>
			</table>
			</td>
			<td class="w50">Acknowledged by the above by:<br /><br />
			<table class="table table-borderless">
				<tr>
					<td>Signature :</td>
					<td>XXXXXXXXXXXX</td>
				</tr>
				<tr>
					<td>Name :</td>
					<td>XXXXXXXXXXXX</td>
				</tr>
				<tr>
					<td>NRIC / Passport No :</td>
					<td>XXXXXXXXXXXX</td>
				</tr>
			</table></td>
		</tr>

	</table>

	<table class="table table-bordered">
		<tr>
			<td colspan="2" class="center"><b>MONTHLY INCOME DECLARATION</b></td>
		</tr>
		<tr>
			<td colspan="2">I am unable to furnish a copy of my income as required. <br /><br /><br /></td>
		</tr>

		<tr>
			<td class="center"><b>HIRER</b></td>
			<td class="center"><b>GUARANTOR</b></td>
		</tr>

		<tr>
			<td class="w50">I hereby declare and confirm that my current monthly income is $XXXXXXXXX<br /><br /><table class="table table-borderless">
				<tr>
					<td>Signature :</td>
					<td>XXXXXXXXXXXX</td>
				</tr>
				<tr>
					<td>Name :</td>
					<td>XXXXXXXXXXXX</td>
				</tr>
				<tr>
					<td>NRIC / Passport No :</td>
					<td>XXXXXXXXXXXX</td>
				</tr>
			</table></td>
			<td class="w50">I hereby declare and confirm that my current monthly income is $XXXXXXXXX<br /><br /><table class="table table-borderless">
				<tr>
					<td>Signature :</td>
					<td>XXXXXXXXXXXX</td>
				</tr>
				<tr>
					<td>Name :</td>
					<td>XXXXXXXXXXXX</td>
				</tr>
				<tr>
					<td>NRIC / Passport No :</td>
					<td>XXXXXXXXXXXX</td>
				</tr>
			</table></td>
		</tr>

	</table>

	</main>
	
</body>

</html>