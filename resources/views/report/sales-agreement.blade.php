<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="/css/app.css">
	<link rel="stylesheet" href="/css/report.css">
</head>
<body>
	<page>
		<header>
			<h1><b>ALL MOTORING PTE LTD</b></h1>
			<h3><b>48 TOH GUAN ROAD EAST #06-99 ENTERPRISE HUB S(608586)</b></h3>
			<h4>SPECIALISE IN LUXURY, SUPERCARDS EXPORT/TRADING & PARTS FOR PORSCHE, FERRARI, MASERATI</h4>
			<h6>HP: 8100 0999 TEL : 6464 6499 FAX: 64646477</h6>

		</header>
		<main>
			<div class="display-inline">
				<h2><b>SALES AGREEMENT</b></h2>
			</div>

			<div class="display-right">
				<p>VSA No:</p>
			</div>

			<div class="clear-right">
				<p>Date  :  16 May 2019</p>
			</div>

			<h5 class="mt20"><b>PARTICULARS OF BUYER:</b></h5>

			<p>By this agreement, I/We XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX NRIC/Passport/Bus. Reg No XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX of having our registered address at XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX. Email :  XXXXXXXXXXXXXXXXXXXXXXXXX Tel No:XXXXXXXXXXXXXXXXXXXXXXXXX Office No: XXXXXXXXXXXXXXXXXXXXXXXXX HP No: XXXXXXXXXXXXXXXXXXXXXXXXX hereby agree to buy from <b>ALL MOTORING PTE LTD</b> (the Vendor) the motor vehicle REGISTRATION NUMBER <span class="end">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</span></p>

			<p>for the sum of Dollars XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX<span class="end">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</span></p>

			<table class="table table-bordered">
				<tr>
					<td><b>PARTICULARS OF VEHICLE:</b></td>
				</tr>
				<tr>
					<td>Make/Model: XXXXXXXXXXXXXXXXXXXXXXX Chassis No: XXXXXXXXXXXXXXXXXXXXXXX YOM: XXXXXXXXXXXXXXXXXXXXXXX Original Reg. Date: XXXXXXXXXXXXXXXXXXXXXXX Engine No: XXXXXXXXXXXXXXXXXXXXXXX No. of Transfer: XXXXXXXXXXXXXXXXXXXXXXX Engine Capacity: XXXXXXXXXXXXXXXXXXXXXXX PARF: XXXXXXX Road Tax Validity: XXXXXXXXXXXXXXXXXXXXXXX COE: XXXXXXXXXXXXXX OMV: XXXXXXXXXXXXXXX Colour: XXXXXXXXXXXXX Others/Accessories: XXXXXXXXXXXXXXXXXXXXXXX</td>
				</tr>
			</table>

			<table class="table table-bordered">
				<tr>
					<td colspan="3"><b>VEHICLE PRICE:</b></td>
				</tr>
				<tr>
					<td class="w70">PRICE AGREED:</td>
					<td class="w20">S$</td>
					<td class="w10"></td>
				</tr>
				<tr>
					<td class="w70">DEPOSIT: Cash/ Nets/ VISA/ Cheque No:</td>
					<td class="w20">S$</td>
					<td class="w10"></td>
				</tr>
				<tr>
					<td class="w70">BALANCE DUE:</td>
					<td class="w20">S$</td>
					<td class="w10"></td>
				</tr>
			</table>

			<table class="table table-bordered">
				<tr>
					<td colspan="3" class="border-bottom"><b>TRADE IN (IF ANY):</b></td>
				</tr>
				<tr>
					<td class="w20 border-bottom">Vehicle No: </td>
					<td class="w40 border-bottom">Make & Model: </td>
					<td class="w20 border-bottom">PRICE: S$</td>
				</tr>
			</table>

			<p class="mt20 fs8">Important notice:</p>
			<p class="fs8">1)  The Buyer hereby acknowledges and confirms that the transaction details contained in Sales Agreement provided to the buyer are subjected to the vendor's managements.</p>
			<p class="fs8">2)  This Sale Agreement is not bind to the vendor unless signed by a director or the manager of Vendor. Any amendments or variants hereof shall not be bind unless the agreement is acknowledged by and countersigned by a Director or the Manager of Vendor.</p>
			<p class="fs8">3)  The Agreed Price is based on financing and insurance arranged by Vendor. The purchase must include a minimum loan financing amount and insurance. The vendor reserves the right to charge a penalty of 2% of the agreed price and S$300 respectively if the finance amount and insurance criteria are not met.</p>
			<p class="fs8">4)  None of the vendor's Sales Representatives or Employees have the Vendor's authority to (a) acquire or arrange for the buyer to acquire scrap cars or the papers thereof, (b) To sell or trade in the buyer's existing car, (c) To sell, trade or install any car accessories not reflected in this agreement, (d) Make any representation or promise, whether oral or in writing retaining to this agreement, (e) accep any monies and give good discount thereof. The vendor shall not be responsible or liable for any arrangement of advice or representation or promise, and shall be strictly personal between the Buyer and the Sale Representative and/or employee and/or such other relevant third party.</p>
			<p class="fs8">5)  The buyer hereby acknowledges and confirms that the Vehicle is sold in a "as is" basis and that the buyer has inspected and tested the Vehicle and is satisfied with and have accepted the actual state and condition thereof (including odometer and all accessories) and that the Buyer has relied wholly on his/her judgment in purchasing the vehicle and shall not raise or be entitled to raise any objection whatsoever here after.</p>
			<p class="fs8">6)  The Vendor does not give any assurances on the vehicle's mileage accuracy and is not liable for any discrepancies (if any) on the vehicle's mileage accuracy.</p>
			<p class="fs8">7)  The Buyer shall be responsible and liable for and shall fully indemnity the vendor from and against any damages, losses, fines, penalties, liabilities, proceedings, costs and expenses howsoever incurred or suffered by the Vendor arising from, as a result of or in connection with the Vehicle after the Buyer has taken delivery of the same (whether the vehicle has been transferred to the Buyer's Name or otherwise.)</p>


			<div class="display-inline">
				<p class="title-overline">Buyer's Signature&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</p>
			</div>

			<div class="display-right">
				<p class="title-overline">Sales Representative's Signature / Name</p>
			</div>
			

			<p class="text-underline"><b>Notes:</b></p>
			<p class="fs8">All deposits are not refundable or transferable. </p>
			<p class="fs8">Cheque payment must be made payable to <b>ALL MOTORING PTE LTD</b></p>
			<p class="fs8">This agreement is not valid unless signed by authorized personnel</p>
		</main>	

	</page>
	
</body>

</html>