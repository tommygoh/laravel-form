<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="/css/app.css">
	<link rel="stylesheet" href="/css/report.css">
</head>
<body>
	<page>
		<header>
			<h1><b>ALL MOTORING PTE LTD</b></h1>
			<h3><b>48 TOH GUAN ROAD EAST #06-99 ENTERPRISE HUB S(608586)</b></h3>
			<h4>SPECIALISE IN LUXURY, SUPERCARDS EXPORT/TRADING & PARTS FOR PORSCHE, FERRARI, MASERATI</h4>
			<h6>HP: 8100 0999 TEL : 6464 6499 FAX: 64646477</h6>

		</header>
		<main>
			<div class="display-inline">
				<h2><b>PURCHASE AGREEMENT</b></h2>
			</div>

			<div class="display-right">
				<p>PAD No:</p>
			</div>

			<div class="clear-right">
				<p>Date  :  16 May 2019</p>
			</div>

			<h5 class="mt20"><b>PARTICULARS OF BUYER:</b></h5>

			<p>By this agreement, I/We XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX NRIC/Passport/Bus. Reg No XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX of having our registered address at XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX. Email :  XXXXXXXXXXXXXXXXXXXXXXXXX Tel No:XXXXXXXXXXXXXXXXXXXXXXXXX Office No: XXXXXXXXXXXXXXXXXXXXXXXXX HP No: XXXXXXXXXXXXXXXXXXXXXXXXX hereby agree to sell to <b>ALL MOTORING PTE LTD</b> (the Purchaser) the motor vehicle REGISTRATION NUMBER <span class="end">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</span></p>

			<p>for the sum of Dollars XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX<span class="end">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</span></p>

			<table class="table table-bordered">
				<tr>
					<td><b>PARTICULARS OF VEHICLE:</b></td>
				</tr>
				<tr>
					<td>Make/Model: XXXXXXXXXXXXXXXXXXXXXXX Chassis No: XXXXXXXXXXXXXXXXXXXXXXX YOM: XXXXXXXXXXXXXXXXXXXXXXX Original Reg. Date: XXXXXXXXXXXXXXXXXXXXXXX Engine No: XXXXXXXXXXXXXXXXXXXXXXX No. of Transfer: XXXXXXXXXXXXXXXXXXXXXXX OMV: XXXXXXX COE: XXXXXXX PARF: XXXXXXX Road Tax Validity: XXXXXXXXXXXXXXXXXXXXXXX Engine Capacity: XXXXXXXXXXXXXX Colour: XXXXXXXXXXXXX Others/Accessories: XXXXXXXXXXXXXXXXXXXXXXX Mileage: XXXXXXXXXXXXXX No of Keys: XXXXXXXXXXXXXX Car Manual: XXXXXXXXXXXXXX</td>
				</tr>
			</table>

			<table class="table table-bordered">
				<tr>
					<td class="w70">PRICE AGREED:</td>
					<td class="w20">S$</td>
					<td class="w10"></td>
				</tr>
				<tr>
					<td class="w70">DEPOSIT: Cash/ Nets/ VISA/ Cheque No:</td>
					<td class="w20">S$</td>
					<td class="w10"></td>
				</tr>
				<tr>
					<td class="w70">FINANCE BALANCE: Finance Co: XXXXXXXXXXXXXXXXX Due Date: XXXXXXXXXXXXX</td>
					<td class="w20">S$</td>
					<td class="w10"></td>
				</tr>
				<tr>
					<td class="w70">OTHERS:</td>
					<td class="w20">S$</td>
					<td class="w10"></td>
				</tr>
				<tr>
					<td class="w70">BALANCE DUE:</td>
					<td class="w20">S$</td>
					<td class="w10"></td>
				</tr>
			</table>

			<p class="fs8">I/We hereby confirm that the vehicle is free from all encumbrances.</p>
			<p class="fs8">I/We hereby confirm that I/We have been informed by the Purchaser and I/We are aware that the above said vehicle may be sent to an authorized inspection centre for evaluation, after which, a Certificate of Inspection will be issued to certify the grade and actual condition of the above said vehicle. By this agreement, I/We confirmed that the above said vehicle is accident free and free from any defects & in good working condition with spare keys. I/We hereby agree to bear the full repair cost in the event that the Certificate of Inspection stated otherwise.</p>
			<p class="fs8">The above said vehicle will be handed over to the Purchaser by XXXXXXXXXXXXXXXXX failing which the Purchaser shall be entitled to rescind the agreement and the Seller shall refund the deposit paid and also compensate the Purchaser three times the amount of deposit which has been paid to the Seller as agreed damages.</p>
			<p class="fs8">Should there be any road tax/ PARF drop expiry, the vehicle must be handed to the Purchaser 14 working days before the expiry date, which is earlier, otherwise we reserve the right to amend the purchase price and/or charge the surcharge accordingly without prior notice.</p>
			<p class="fs8">I/We acknowledge receipt of the deposit payment & agree to the terms & conditions stated above.</p>

			<p class="mt20">Remarks : XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX</p>

			<p class="mt20">Date / Time of Delivery : XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX</p>

			<div class="display-inline">
				<p class="title-overline">Cash/Chq No:&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</p>
			</div>

			<div class="display-inline">
				<p class="title-overline">Signature of Purchaser&emsp;&emsp;&emsp;</p>
			</div>

			<div class="display-inline">
				<p class="title-overline">Signature of Seller&emsp;&emsp;&emsp;</p>
			</div>

			<div class="display-right">
				<p class="title-overline">Attended by&emsp;&emsp;&emsp;</p>
			</div>
			

			<p class="text-underline"><b>Notes:</b></p>
			<p class="fs8">All deposits are not refundable or transferable. </p>
			<p class="fs8">Cheque payment must be made payable to <b>ALL MOTORING PTE LTD</b></p>
			<p class="fs8">This agreement is not valid unless signed by authorized personnel</p>
		</main>	

	</page>
	
</body>

</html>