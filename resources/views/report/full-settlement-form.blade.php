<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="/css/app.css">
	<link rel="stylesheet" href="/css/report.css">
</head>
<body>
	<page>
		<main>
			
			<p>PRIVILEGE CAPITAL / MOTORS PTE LTD</p>

			<p class="mt10">37 Jalan Pemimpin</p>

			<p class="mt10">#01-01, Mapex</p>

			<p class="mt10">Singapore 577177</p>

			<p class="mt40">Dear Sir / Mdm,</p>

			<p class="mt20"><b>RE: Full Settlement for Floor Stock</b></p>

			<p class="mt20">Please confirm and release the amount payable to us with regards to the below-mentioned vehicle:=</p>

			<table class="table table-borderless w50">
				<tr>
					<td class="w30">Vehicle Reg No:</td>
					<td class="w70" colspan="3"><p class="text-underline">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</p></td>
				</tr>
				<tr>
					<td class="w30">Principal Floor Amt:</td>
					<td class="w70" colspan="3"><p class="text-underline">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</p></td>
				</tr>
				<tr>
					<td>Loan Amt:</td>
					<td><p class="text-underline">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</p></td>
					<td class="w30">Bank:</td>
					<td><p class="text-underline">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</p></td>
				</tr>
				<tr>
					<td>Payment Date:</td>
					<td colspan="3"><p class="text-underline">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</p></td>
				</tr>

			</table>

			<p class="mt40">I understand that transfer of ownership will be done 1 working day for full redemption and 2 working days for contra cases. Payment is to be made <b>ONLY</b> in cheque and before 2.00pm on the due date.</p>


			<p class="mt20">Yours Sincerely,</p>

			<p class="title-overline">Authorized Signatory</p>
		</main>	

	</page>
	
</body>

</html>