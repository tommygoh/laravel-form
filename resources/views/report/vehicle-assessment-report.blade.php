<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="/css/app.css">
	<link rel="stylesheet" href="/css/report.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>
<body>
	<page>
		
		<header>
			<h1><b>ALL MOTORING PTE LTD</b></h1>
			<h3>48 TOH GUAN ROAD EAST #06-99 ENTERPRISE HUB S(608586)</h3>
			<h3>TEL: 8100 0999 TEL : 6464 6499 FAX: 64646477</h3>

		</header>
		<main>
			
			<h4 class="mt20"><b>Standard Vehicle Assessment Report</b></h4>

			<table class="table table-borderless">
				<tr>
					<td class="w20"><p>Checklist for Vehicle Examination</p></td>
					<td class="w30"></td>
					<td class="w50"><p><b>Legend: <i class="material-icons" style="font-size:18px;">check</i>- Satisfactory&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; X - Unsatisfactory</b></p></td>
				</tr>
			</table>

			<table class="table table-bordered-no-padding w45">
				<thead class="thead-light">
					<tr>
				      <th><b>A</b></th>
				      <th colspan="2"><b>VEHICLE DETAILS</b></th>
				    </tr>
				</thead>
				<tbody>
					<tr>
						<td class="w10">1</td>
						<td class="w60">Registration Number</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">2</td>
						<td class="w60">Make</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">3</td>
						<td class="w60">Model</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">4</td>
						<td class="w60">Capacity</td>
						<td class="w30">&emsp;&emsp;&emsp;&emsp;&emsp;cc</td>
					</tr>
					<tr>
						<td class="w10">5</td>
						<td class="w60">Mileage</td>
						<td class="w30">&emsp;&emsp;&emsp;&emsp;&emsp;km</td>
					</tr>
				</tbody>
				
			</table>

			<table class="table table-bordered-no-padding w45">
				<tbody>
					<tr>
						<td class="w10">6</td>
						<td class="w60">Date Of Registration</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">7</td>
						<td class="w60">Propellant</td>
						<td class="w30">Petrol / Diesel</td>
					</tr>
					<tr>
						<td class="w10">8</td>
						<td class="w60">Chassis Number</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">9</td>
						<td class="w60">Engine Number</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">10</td>
						<td class="w60">IU Number</td>
						<td class="w30"></td>
					</tr>
				</tbody>
				
			</table>

			<table class="table table-bordered-no-padding w45">
				<thead class="thead-light">
					<tr>
				      <th><b>B</b></th>
				      <th colspan="2"><b>VISUAL CHECKS</b></th>
				    </tr>
				</thead>
				<tbody>
					<tr>
						<td class="w10">1</td>
						<td class="w60">Headlamps</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">2</td>
						<td class="w60">Tail Lamp</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">3</td>
						<td class="w60">Brake Lamp</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">4</td>
						<td class="w60">Reserve Lamp</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">5</td>
						<td class="w60">Directional Indicator Lamps</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">6</td>
						<td class="w60">License Plate Lamp</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">7</td>
						<td class="w60">Wiper</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">8</td>
						<td class="w60">Malfunction Indicator Lamp</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">9</td>
						<td class="w60">Power Window</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">10</td>
						<td class="w60">Hazard Lights</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">11</td>
						<td class="w60">Child Lock</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">12</td>
						<td class="w60">Engine</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">13</td>
						<td class="w60">Engine Mounting</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">14</td>
						<td class="w60">Transmission Box</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">15</td>
						<td class="w60">Drive Shaft & Covers</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">16</td>
						<td class="w60">Power Steering Unit</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">17</td>
						<td class="w60">Steering Linkages</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">18</td>
						<td class="w60">Cooling System</td>
						<td class="w30"></td>
					</tr>
				</tbody>
				
			</table>

			<table class="table table-bordered-no-padding w45">
				<tbody>
					<tr>
						<td class="w10">19</td>
						<td class="w60">Body Work</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">20</td>
						<td class="w60">Brake Rotor</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">21</td>
						<td class="w60">Brake Lines</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">22</td>
						<td class="w60">Brake Linkages</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">23</td>
						<td class="w60">Auxiliary Belt (Fan)</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">24</td>
						<td class="w60">Exhaust System</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">25</td>
						<td class="w60">Tyre and Rims</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">26</td>
						<td class="w60">Suspension Linkages</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">27</td>
						<td class="w60">Undercarriage</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">28</td>
						<td class="w60">Engine Oil</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">29</td>
						<td class="w60">Transmission Oil</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">30</td>
						<td class="w60">Power steering Oil</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">31</td>
						<td class="w60">Coolant</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">32</td>
						<td class="w60">Shock Absorbers</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">33</td>
						<td class="w60">Brake Fluid</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">34</td>
						<td class="w60">Horn</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">35</td>
						<td class="w60">Air-Con Compressor</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">36</td>
						<td class="w60">Safety Belts</td>
						<td class="w30"></td>
					</tr>
				</tbody>
				
			</table>

			<table class="table table-bordered-no-padding w45">
				<thead class="thead-light">
					<tr>
				      <th><b>C</b></th>
				      <th colspan="2"><b>EQUIPMENT CHECKS</b></th>
				    </tr>
				</thead>
				<tbody>
					<tr>
						<td class="w10">37</td>
						<td class="w60">Headlight</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">38</td>
						<td class="w60">Sound Emission</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">39</td>
						<td class="w60">Brake</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">40</td>
						<td class="w60">Exhaust Emission</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">41</td>
						<td class="w60">Side Slip</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">42</td>
						<td class="w60">Air Conditioning</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">43</td>
						<td class="w60">Battery</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">44</td>
						<td class="w60">Alternator</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">45</td>
						<td class="w60">IU</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">46</td>
						<td class="w60">Tyre</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">47</td>
						<td class="w60">Windscreen and Windows</td>
						<td class="w30"></td>
					</tr>
				</tbody>
				
			</table>

			<table class="table table-bordered-no-padding w45">
				<tbody>
					<tr>
						<td class="w10">48</td>
						<td class="w60">Engine</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">49</td>
						<td class="w60">Transmission</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">50</td>
						<td class="w60">Torque Converter</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">51</td>
						<td class="w60">Abnormal Noise</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">52</td>
						<td class="w60">Shock Absorbers</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">53</td>
						<td class="w60">Drive Shaft</td>
						<td class="w30"></td>
					</tr>
					<tr>
						<td class="w10">54</td>
						<td class="w60">Wheel Bearings</td>
						<td class="w30"></td>
					</tr>
				</tbody>
			</table>

			<table class="table table-borderless">
				<tr>
					<td class="w10 fs12">Remarks :</td>
					<td class="w90 fs12"><p class="blank-underline">&emsp;</p></td>
				</tr>
			</table>

			<p class="mt120">Exclusion of Liability and Indemnity</p>

			<p>The inspection of the vehicle is conducted jointly with the Customer. Save for any non-conformity or defect listed above, the Customer has found the vehicle to be satisfactory and merchantable quality, in good road-worthy condition and fit for its purpose as a motor vehicle. The Customer does not have any particular purpose for the vehicle other than as a motor vehicle.</p>

			<p class="mt20">The Customer shall release and hold harmless the Seller, its employees, servants and agents from all claims by the Customer or from any other person(s) for any loss, damage, injury cost or expenses of whatever nature arising or resulting from any neglect, omission or default on the part of Seller, its employees, servants or agents, from any information or opinion given or expressed in this report or from any other causes save for any gross negligence on the part of the Seller, its employees, servants and agents.</p>

			<p class="mt20">In the event that any claim is made against the Seller, its employees, servants and agents by the Customer or any other person(s) for any loss, damage, injury costs or expenses of whatever nature arising or resulting from the above causes, the Customer shall indemnify and keep the Seller, its employees, servants and agents fully indemnified against any such claim unless it relate to non-conformity or defect discloses in the inspection in which event the lemon law will be applicable.</p>

			<p class="text-underline mt40"><b>Confidentiality of Report</b></p>
			<p>This report is strictly private and confidential, and shall be used solely and exclusively by the Seller and/or the Customer for their benefit. This report shall not be distributed, disclosed or used by any other third party without the prior written consent of the Seller and/or the Customer. This standard vehicle assessment report is recommended by Singapore Vehicle Traders Association (SVTA) for SVTA's members only and is conducted for the purpose of the certifying on the condition of vehicle</p>

			<p>I agree to the foregoing conditions of my vehicle. I also confirm that any visible defect to the vehicle was pointed out to me before I agreed to buy the motor-vehicle.</p>

			<p class="mt40 right">Confirmed and Accepted by:</p>

			<table class="table table-borderless w45 mt80">
				<tr>
					<td colspan="2"><p class="text-underline"><b>Company</b></p></td>
				</tr>
				<tr>
					<td class="w40">Name : </td>
					<td class="w60 td-border-bottom"><p class="w100 text-underline">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</p></td>
				</tr>
				<tr>
					<td class="w40">ROC No. : </td>
					<td class="w60"><p class="w100 text-underline">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</p></td>
				</tr>	
				<tr>
					<td class="w40">Address : </td>
					<td class="w60"><p class="w100 text-underline">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</p></td>
				</tr>
				<tr>
					<td class="w40"></td>
					<td class="w60"><p class="w100 text-underline">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</p></td>
				</tr>		
				<tr>
					<td class="w40">Signature : </td>
					<td class="w60"><p class="mt40 w100 text-underline">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</p></td>
				</tr>	
				<tr>
					<td class="w40">Date : </td>
					<td class="w69"><p class="w100 text-underline">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</p></td>
				</tr>		
			</table>

			<table class="table table-borderless w45 ml40">
				<tr>
					<td colspan="2"><p class="text-underline"><b>Customer</b></p></td>
				</tr>
				<tr>
					<td class="w40">Name : </td>
					<td class="w60 td-border-bottom"><p class="w100 text-underline">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</p></td>
				</tr>
				<tr>
					<td class="w40">NRIC No. : </td>
					<td class="w60"><p class="w100 text-underline">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</p></td>
				</tr>	
				<tr>
					<td class="w40">Address : </td>
					<td class="w60"><p class="w100 text-underline">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</p></td>
				</tr>
				<tr>
					<td class="w40"></td>
					<td class="w60"><p class="w100 text-underline">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</p></td>
				</tr>		
				<tr>
					<td class="w40">Signature : </td>
					<td class="w60"><p class="mt40 w100 text-underline">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</p></td>
				</tr>	
				<tr>
					<td class="w40">Date : </td>
					<td class="w69"><p class="w100 text-underline">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</p></td>
				</tr>		
			</table>

		</main>	

	</page>
	
</body>

</html>