<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="/css/app.css">
	<link rel="stylesheet" href="/css/report.css">
</head>
<body>
	<page>
		
		<header>
			<h1><b>ALL MOTORING PTE LTD</b></h1>
			<h3><b>48 TOH GUAN ROAD EAST #06-99 ENTERPRISE HUB S(608586)</b></h3>
			<h5>SPECIALISE IN LUXURY, SUPERCARDS EXPORT/TRADING & PARTS FOR PORSCHE, FERRARI, MASERATI</h5>
			<h6>HP: 8100 0999 TEL : 6464 6499 FAX: 64646477</h6>

		</header>
		<main>
			
			<h4 class="title-underline"><b>AUTHORIZED LETTER</b></h4>

			<table class="table table-borderless w70">
				<tr>
					<td>Vehicle Particulars:-<br /></td>
					<td></td>
				</tr>
				<tr>
					<td>Registration Number:</td>
					<td>SKH4141C</td>
				</tr>
				<tr>
					<td>Make / Model:</td>
					<td>HONDA CIVIC 1.8L 5AT</td>
				</tr>
			</table>

			<p class="mt20"> We, hereby authorize <b>KENSO LEASING PTE LTD</b> to settle this vehicle on behalf for <b>ALLMOTORING PTE LTD</b> with <b>TECK WEI CREDIT PTE LTD</b></p>

			<p class="mt20"> Full settlement amount: XXXXXXXXXXXX</p>

			<p class="mt20">The details of this car are as follows:</p>

			<table class="table table-borderless w70">
				<tr>
					<td>1) Make and Model: </td>
					<td>HONDA CIVIC 1.8L 5AT</td>
				</tr>
				<tr>
					<td>2) Chassis Number: </td>
					<td>JHMFD16309S200825</td>
				</tr>
				<tr>
					<td>3) Engine Number: </td>
					<td>R18A14004918</td>
				</tr>
			</table>

			<p class="mt20">We hereby authorized <b>TECK WEI CREDIT PTE LTD</b> to E-transfer the above vehicle to <b>KENSO LEASING PTE LTD</b></p>

			<p class="mt20"> Kindly assist the transfer upon receipt of full payment of the settlement.</p>

			<p class="mt20">Yours Faithfully,</p>

			<p class="title-overline">Authorized Signature</p>
		</main>	

	</page>
	
</body>

</html>