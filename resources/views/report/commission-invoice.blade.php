<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="/css/app.css">
	<link rel="stylesheet" href="/css/report.css">
</head>
<body>
	<page>
		
		<header>
			<h1><b>ALL MOTORING PTE LTD</b></h1>
			<h3><b>48 TOH GUAN ROAD EAST #06-99 ENTERPRISE HUB S(608586)</b></h3>
			<h5>SPECIALISE IN LUXURY, SUPERCARDS EXPORT/TRADING & PARTS FOR PORSCHE, FERRARI, MASERATI</h5>
			<h6>HP: 8100 0999 TEL : 6464 6499 FAX: 64646477</h6>

		</header>
		<main>
			
			<h4 class="title-underline"><b>INVOICE NO. 2288</b></h4>

			<div class="display-inline">
				<p>Date:</p>
			</div>
			<div class="display-inline">
				<p>04-Jan-19</p>
			</div>

			<p class="mt20"><b>KENSO LEASING PTE LTD</b></p>

			<p>NO 151 CHIN SWEE ROAD</p>

			<p>MANHATTAN HOUSE</p>

			<p>#13-07 SINGAPORE 169876</p>

			<table class="table table-borderless w70">
				<tr>
					<td>For The Account Of :</td>
					<td>JIN SIN MAY PATRICIA</td>
				</tr>
				<tr>
					<td>NRIC No. / Business Reg No. :</td>
					<td>S1795595J</td>
				</tr>
			</table>

			<div class="hr-line"></div>

			<table class="table table-borderless w70">
				<tr>
					<td colspan="2">Being Sale of:- <br /></td>
				</tr>
				<tr>
					<td>Make & Model :</td>
					<td>HONDA CIVIC 1.8L 5AT</td>
				</tr>
				<tr>
					<td>Engine Number :</td>
					<td>R18A14004918</td>
				</tr>
				<tr>
					<td>Chassis Number :</td>
					<td>JHMFD16309S200825</td>
				</tr>
				<tr>
					<td>Registration Number :</td>
					<td>SKH4141C</td>
				</tr>
				<tr>
					<td>Year of Manufacture :</td>
					<td>2008</td>
				</tr>
				<tr>
					<td>Tenor (Months) :</td>
					<td>54</td>
				</tr>
				<tr>
					<td>Bank :</td>
					<td>KENSO LEASING PTE LTD</td>
				</tr>
				<tr>
					<td>Loan Amount :</td>
					<td><b>$ 33,800.00</b></td>
				</tr>
				<tr>
					<td>Commission :</td>
					<td>$507.00</td>
				</tr>

			</table>

			<p class="mt40">We certify that the particulars in the invoice are true and correct and the above vehicle is our sole right property unencumbered and that I have the right to sell it free from any lien.</p>

			<p class="title-overline">Authorized Signature</p>
		</main>	

	</page>
	
</body>

</html>