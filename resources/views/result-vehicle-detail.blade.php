@extends('layouts.app')

@section('content')
	@if(session('no'))
		<div class="alert alert-danger">
			{{session('no')}}
		</div>
	@endif
	@if(session('arrayTitle'))
		<table class="table">
		    @for ($x = 0; $x <= count(session('arrayTitle')) - 1; $x++)
	        	<tr>
	        		<td> 
	        			{{session('arrayTitle')[$x]}}
	        		</td>
	        		<td> 
	        			{{session('arrayResult')[$x]}}
	        		</td>
	        	</tr>
		    @endfor
		</table>
	@endif
@endsection