@extends('layouts.app')

@section('content')
	<h1>Form</h1>
	{!! Form::open(['url' => '/roadtax']) !!}
		<div class="form-group">
			{{Form::label('vehicle', 'Vehicle Number')}}
			{{Form::text('vehicle', '',['class' => 'form-control'])}}
		</div>
		<div class="form-group">
			{{Form::label('ownerId', 'Owner ID')}}
			{{Form::text('ownerId', '',['class' => 'form-control'])}}
		</div>
		<div>
			<iframe width="200" height="100" srcdoc="{{$output}}"></iframe>
		</div>
		<div class="form-group">
			{{Form::label('captcha', 'Enter the text shown above')}}
			{{Form::text('captcha', '',['class' => 'form-control'])}}
			{{ Form::hidden('cookie', $cookie) }}
		</div>
		<div>
			{{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
		</div>
	{!! Form::close() !!}
@endsection