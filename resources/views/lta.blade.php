@extends('layouts.app')

@section('content')
	<h1>Form</h1>
	{!! Form::open(['url' => '/lta']) !!}
		<div class="form-group">
			{{Form::label('vehicle', 'Vehicle Number')}}
			{{Form::text('vehicle', '',['class' => 'form-control'])}}
		</div>
		<div>
			{{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
		</div>
	{!! Form::close() !!}
@endsection