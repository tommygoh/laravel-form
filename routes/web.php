<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('form');
// });

Route::get('/', 'VehicleController@getForm');

Route::get('/lta', function () {
    return view('lta');
});

Route::get('/acknowledge-receipt', function () {
    return view('report.acknowledge-receipt');
});

Route::get('/sales-agreement', function () {
    return view('report.sales-agreement');
});

Route::get('/purchase-agreement', function () {
    return view('report.purchase-agreement');
});

Route::get('/aml', function () {
    return view('report.aml');
});

Route::get('/company-authorization-letter', function () {
    return view('report.company-authorization-letter');
});

Route::get('/company-transfer-authorization', function () {
    return view('report.company-transfer-authorization');
});

Route::get('/full-settlement-form', function () {
    return view('report.full-settlement-form');
});

Route::get('/invoice-full', function () {
    return view('report.invoice-full');
});

Route::get('/commission-invoice', function () {
    return view('report.commission-invoice');
});

Route::get('/vehicle-collection-form', function () {
    return view('report.vehicle-collection-form');
});

Route::get('/vehicle-assessment-report', function () {
    return view('report.vehicle-assessment-report');
});

Route::post('/roadtax', 'VehicleController@getRoadTax');

Route::post('/lta', 'VehicleController@getLta');

Route::get('/result', function () {
    return view('result');
});

Route::post('/result', function() {
    return view('result');
})->name('result');

Route::get('/result-vehicle-detail', function () {
    return view('result-vehicle-detail');
});

Route::post('/result-vehicle-detail', function() {
    return view('result-vehicle-detail');
})->name('result-vehicle-detail');

